const UsuarioController = require('../Controllers/UsuarioController');
const morgan = require('morgan');
const bodyParser = require('body-parser');


module.exports = (app) => {
   app.use(bodyParser.urlencoded({extended:false}));
   app.use(bodyParser.json());
   app.post('/usuario', UsuarioController.post);
   app.put('/usuario/:id', UsuarioController.put);
   app.delete('/usuario/:id', UsuarioController.delete);
   app.get('/usuarios', UsuarioController.get);
   app.get('/usuario/:id', UsuarioController.getById);

   app.use(morgan('dev')); 
   app.use((req,res,next)=> {
      const erro = new Error('Não encontrado');
      erro.status = 404;
      res.status(404).send(`${erro.status} endereço não encontrado`);
   })

   app.use((req,res,next)=>{
      res.header('Access-Control-Allow-Origin','*');
      res.header('Access-Control-Allow-Header',
      'Origin, X-Requrested-With, Content-Type, Accept, Authorization');
      if(req.method === 'OPTIONS'){
         res.header('Access-Control-Allow-Methods','PUT, POST, PATCH, DELETE, GET');
         return res.status(200).send({});
      }
      next();
   });

}