//Constante LivrosController recebe o módulo LivrosController
const LivrosController = require('../Controllers/LivrosController');
// O pacote morgan do NPM é instalado e requisitado para a sequência do processo
const morgan = require('morgan');
//O pacote bodyParser do NPM é instalado para facilitar o retorno das requisições
const bodyParser = require('body-parser');

//as rotas definidas e parametrizadas no controller são inicializada a medida que solicitadas 
module.exports = (app) => {
   app.use(bodyParser.urlencoded({extended:false}));
   app.use(bodyParser.json());
   app.post('/livro', LivrosController.post);
   app.put('/livro/:id', LivrosController.put);
   app.delete('/livro/:id', LivrosController.delete);
   app.get('/livros', LivrosController.get);
   app.get('/livro/:id', LivrosController.getById);

   //O módulo morgan redireciona o fluxo do programa para as rotas não encontradas
   app.use(morgan('dev')); 
   app.use((req,res,next)=> {
      const erro = new Error('Não encontrado');
      erro.status = 404;
      res.status(404).send(`${erro.status} endereço não encontrado`);
   })

   
   app.use((req,res,next)=>{
      res.header('Access-Control-Allow-Origin','*');
      res.header('Access-Control-Allow-Header',
      'Origin, X-Requrested-With, Content-Type, Accept, Authorization');
      if(req.method === 'OPTIONS'){
         res.header('Access-Control-Allow-Methods','PUT, POST, PATCH, DELETE, GET');
         return res.status(200).send({});
      }
      next();
   });

}


