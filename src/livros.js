
 livros = [
  {
    id: 0,
    titulo: 'Curso de Banco de Dados',
    autor: 'João',
    paginas: 120
  },
  {
    id: 1,
    titulo: 'Curso de JavaScript',
    autor: 'Maria',
    paginas: 1300
  },
  {
    id: 2,
    titulo: 'Curso de Angular 2',
    autor: 'José',
    paginas: 450
  },
  {
    id: 3,
    titulo: 'Curso de NodeJS',
    autor: 'Francisco',
    paginas: 15
  }
]



module.exports = livros;




