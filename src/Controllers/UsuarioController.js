//Constante usuários recebe o array de usuários
const usuarios = require('../usuarios');

//O crud é inicializado com as principais rotas
// O rota post tem como requisição /usuários e retorna o array de usuários
exports.post = (req, res, next) => {
    res.status(201).send(usuarios);
 };
 
  // O rota put tem como requisição /usuários/id e retorna o usuários alterado
 //Todos os campos do array são testados se estão presentes na resposta
 exports.put = (req, res, next) => {
   
    let id = req.params.id;
    let status = 0;  
   const usuario = {
      id: req.body.id,
      usuario: req.body.user,
      nome: req.body.nome,
      idade: req.body.idade,
   }
    if(usuario.id){
      let id_user = usuario.id;
      usuarios[id].id = id_user;
       status = 200;
    }
    if(usuario.usuario){
      let user = usuario.usuario;
      let status = 0;
      usuarios[id].usuario = user;
       status = 200;
      }
    if(usuario.nome){
      let nome = usuario.nome;
      usuarios[id].nome = nome;
       status = 200;
    }
    if(usuario.idade){
      let idade = usuario.idade;
      usuarios[id].idade = idade;
      status = 200;
    }
    if(status !== 200){
      //res.status = 550
      res.send('Nenhum campo foi encontrado')
    }else{
      res.status(200).send(`Usuário ${usuarios[id].nome} cadatrado com sucesso`);
   }
   
 };

  //A rota delete tem como requisição o id do usuário e exclui o usuário com o id enviado 
 //retorna o array atual
 exports.delete = (req, res, next) => {
    let id = req.params.id;
    usuarios.splice(id, 1);
    res.status(200).send(usuarios);
 };

 //A rota get tem como requisição /usuários e retorna todos os usuários do array
 exports.get = (req, res, next) => {
    res.status(200).send(usuarios);
 };
  
 //A rota getById tem como requisição o id do usuário e retorna os dados do livro solicitado
 exports.getById = (req, res, next) => {
    let id = req.params.id;
    let user = usuarios[id];
    if(user == null){
       res.status(404).send("Usuário não localizado");
    }else{
      res.status(200).send(usuarios[id]);
    }
 };

