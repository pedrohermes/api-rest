//Constante livros recebe o array de livros
const livros = require('../livros');

//O crud é inicializado com as principais rotas
// O rota post tem como requisição /livors e retorna o array de livro
exports.post = (req, res, next) => {
    res.status(201).send(livros);
 };
 
 // O rota put tem como requisição /livor/id e retorna o livro alterado
 //Todos os campos do array são testados se estão presentes na reequisição
  exports.put = (req, res, next) => {
   
    let id = req.params.id;
    let status = 0;  
   const livro = {
      id: req.body.id,
      titulo: req.body.titulo,
      autor: req.body.autor,
      paginas: req.body.paginas,
   }
    if(livro.id){
      let id_livro = livro.id;
      livros[id].id = id_livro;
      status = 200;
    }
    if(livro.titulo){
      let titulo = livro.titulo;
       livros[id].titulo = titulo;
       status = 200;
      }
    if(livro.autor){
      let autor= livro.autor;
      livros[id].autor = autor;
      status = 200;
    }
    if(livro.paginas){
      let paginas = livro.paginas;
      livros[id].paginas = paginas;
      status = 200;
    }
    if(status !== 200){
      res.send('Nenhum campo foi encontrado')
    }else{
      res.status(200).send(`Livro ${livros[id].titulo} cadatrado com sucesso`);
   }
   
 };
 //A rota delete tem como requisição o id do livro e exclui o livro com o id enviado 
 //retorna o array atual
 exports.delete = (req, res, next) => {
    let id = req.params.id;
    livros.splice(id, 1);
    res.status(200).send(livros);
 };
  
 //A rota get tem como requisição /livros e retorna todos os livros do array
 exports.get = (req, res, next) => {
    res.status(200).send(livros);
 };
  
 //A rota getById tem como requisição o id do livro e retorna os dados do livro solicitado
 exports.getById = (req, res, next) => {
    let id = req.params.id;
    let livro = livros[id];
    if(livro == null){
       res.status(404).send("Livro não localizado");
    }else{
      res.status(200).send(livros[id]);
    }
 };

