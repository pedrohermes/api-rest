//Constante express instanciando o módulo express para utulizar suas funções
// Constante cors recebe o módulo cors para retornar json
//
const express = require('express');
const cors = require('cors');
const app = express();

//constante app é inicializada na porta 3333
require('./src/Routes/index')(app); 
 
app.use(cors());
app.use(express.json());
app.listen(3333);